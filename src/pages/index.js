import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'
// import Lightbox from 'react-images'
import Gallery from '../components/Gallery'

import thumb01 from '../assets/images/thumbs/01.jpg'
import thumb02 from '../assets/images/thumbs/02.jpg'
import thumb03 from '../assets/images/thumbs/03.jpg'
import thumb04 from '../assets/images/thumbs/04.jpg'
import thumb05 from '../assets/images/thumbs/05.jpg'
import thumb06 from '../assets/images/thumbs/06.jpg'
import thumb07 from '../assets/images/thumbs/07.png'
import thumb08 from '../assets/images/thumbs/08.png'
import thumb09 from '../assets/images/thumbs/09.png'
import thumb10 from '../assets/images/thumbs/10.png'
import thumb11 from '../assets/images/thumbs/11.png'

import full01 from '../assets/images/fulls/01.jpg'
import full02 from '../assets/images/fulls/02.jpg'
import full03 from '../assets/images/fulls/03.jpg'
import full04 from '../assets/images/fulls/04.jpg'
import full05 from '../assets/images/fulls/05.jpg'
import full06 from '../assets/images/fulls/06.jpg'
import full07 from '../assets/images/fulls/07.png'
import full08 from '../assets/images/fulls/08.png'
import full09 from '../assets/images/fulls/09.png'
import full10 from '../assets/images/fulls/10.png'
import full11 from '../assets/images/fulls/11.png'

const DEFAULT_IMAGES = [
    // { id: '1', source: full01, thumbnail: thumb01, caption: 'Photo 1', description: 'Lorem ipsum dolor sit amet nisl sed nullam feugiat.'},
    // { id: '2', source: full02, thumbnail: thumb02, caption: 'Photo 2', description: 'Lorem ipsum dolor sit amet nisl sed nullam feugiat.'},
    // { id: '3', source: full03, thumbnail: thumb03, caption: 'Photo 3', description: 'Lorem ipsum dolor sit amet nisl sed nullam feugiat.'},
    // { id: '4', source: full04, thumbnail: thumb04, caption: 'Photo 4', description: 'Lorem ipsum dolor sit amet nisl sed nullam feugiat.'},
    // { id: '5', source: full05, thumbnail: thumb05, caption: 'Photo 5', description: 'Lorem ipsum dolor sit amet nisl sed nullam feugiat.'},
    // { id: '6', source: full06, thumbnail: thumb06, caption: 'Photo 6', description: 'Lorem ipsum dolor sit amet nisl sed nullam feugiat.'},
    { id: '7', source: full07, thumbnail: thumb07, caption: 'Landing Page', description: 'Gatsby, React.js', link: 'https://gatsbyjs-starter-tailwindplay.appseed.us/'},
    { id: '8', source: full08, thumbnail: thumb08, caption: 'Shopping Cart', description: 'Gatsby, JAM stack, Markdown', link: 'https://gatsby-shopify-starter.netlify.com/'},
    { id: '9', source: full09, thumbnail: thumb09, caption: 'Admin Dashboard', description: 'Gatsby', link: 'https://gatsby-starter-paperbase.netlify.com/'},
    { id: '10', source: full10, thumbnail: thumb10, caption: 'Responsive One Page', description: 'Bootstrap 4, SASS/SCSS', link: 'https://merantau.gitlab.io/one-page-responsive/ '},
    { id: '11', source: full11, thumbnail: thumb11, caption: 'SaaS Landing', description: 'Gatsby', link: 'https://gatsby-starter-saas-marketing.netlify.com/'}
];

class HomeIndex extends React.Component {

    render() {
        const siteTitle = "Portfolio | Tareq"
        const siteDescription = "Site description"

        return (
            <Layout>
                <Helmet>
                        <title>{siteTitle}</title>
                        <meta name="description" content={siteDescription} />
                </Helmet>

                <div id="main">

                    <section id="one">
                        <header className="major">
                            <h2>Full stack web<br />
                            and mobile app developer.</h2>
                        </header>
                        <p>❝Tareq did excellent work. He completed the project quickly and asked important questions and gave suggestions for improvements. I'll be hiring him again.❞</p>
                        <p>❝Tareq was very professional with good communication. He understood the brief perfectly and completed the project to a high standard within the deadline. It's hard to find developers with a high level of professionalism. Thanks for your help Tareq, I will be in-touch again!❞</p>
                        <ul className="actions">
                            <li><a href="/testimonials" className="button">Learn More</a></li>
                        </ul>
                    </section>

                    <section id="two">
                        <h2>Recent Work</h2>

                        <Gallery images={DEFAULT_IMAGES.map(({ id, source, thumbnail, caption, description, link }) => ({
                            source,
                            thumbnail,
                            caption,
                            description,
                            link
                        }))} />

                        <ul className="actions">
                            <li><a href="/" className="button">Full Portfolio</a></li>
                        </ul>
                    </section>

                    <section id="three">
                        <h2>Get In Touch</h2>
                        <p>Request to build awesome web projects.</p>
                        <div className="row">
                            <div className="8u 12u$(small)">
                                <form method="post" action="#">
                                    <div className="row uniform 50%">
                                        <div className="6u 12u$(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
                                        <div className="6u 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
                                        <div className="12u"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
                                    </div>
                                </form>
                                <ul className="actions">
                                    <li><input type="submit" value="Send Message" disabled/></li>
                                </ul>
                            </div>
                            <div className="4u 12u$(small)">
                                <ul className="labeled-icons">
                                    <li>
                                        <h3 className="icon fa-home"><span className="label">Address</span></h3>
                                        Helsinki,<br />
                                        Finland
                                    </li>
                                    <li>
                                        <h3 className="icon fa-mobile"><span className="label">Phone</span></h3>
                                        ***-***-****
                                    </li>
                                    <li>
                                        <h3 className="icon fa-envelope-o"><span className="label">Email</span></h3>
                                        <a href="mailTo:tareqsolutions@gmail.com">tareqsolutions@gmail.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>

                </div>

            </Layout>
        )
    }
}

export default HomeIndex