import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'

class Testimonials extends React.Component {

    render() {
        const siteTitle = "Testimonials | Tareq"
        const siteDescription = "Site description"

        return (
            <Layout>
                <Helmet>
                        <title>{siteTitle}</title>
                        <meta name="description" content={siteDescription} />
                </Helmet>

                <div id="main">

                    <section id="one">
                        <header className="major">
                            <h2>My clients say<br />
                            and other developers cry.</h2>
                        </header>
                        <p>❝Tareq did excellent work. He completed the project quickly and asked important questions and gave suggestions for improvements. I'll be hiring him again.❞</p>
                        <p>❝Tareq was very professional with good communication. He understood the brief perfectly and completed the project to a high standard within the deadline. It's hard to find developers with a high level of professionalism. Thanks for your help Tareq, I will be in-touch again!❞</p>
                        <p>❝Tareq is a one of the great persons that I met on ******. He provided his working result on time with high quality. I hope to work with him again. Thanks. A+ ❞</p>
                        <p>❝Very good and helpful person. hope to see him in our next projects!❞</p>
                        <p>❝I have had a great experience working with Tareq. Tareq responded fast and implemented our requests related to LearnDash customization in just a few hours. It could not have been done better. Thanks Tareq.❞</p>
                        <ul className="actions">
                            <li><a href="/" className="button">Back to Home</a></li>
                        </ul>
                    </section>

                    <section id="two">
                        <h2>From developers</h2>

                        <p>❝He's monster!❞</p>
                        <p>❝Love to work with Tareq❞</p>

                        <ul className="actions">
                            <li><a href="/" className="button">Back to Home</a></li>
                        </ul>
                    </section>

                    <section id="three">
                        <h2>Get In Touch</h2>
                        <p>Request to build awesome web projects.</p>
                        <div className="row">
                            <div className="8u 12u$(small)">
                                <form method="post" action="#">
                                    <div className="row uniform 50%">
                                        <div className="6u 12u$(xsmall)"><input type="text" name="name" id="name" placeholder="Name" /></div>
                                        <div className="6u 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
                                        <div className="12u"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
                                    </div>
                                </form>
                                <ul className="actions">
                                    <li><input type="submit" value="Send Message" disabled/></li>
                                </ul>
                            </div>
                            <div className="4u 12u$(small)">
                                <ul className="labeled-icons">
                                    <li>
                                        <h3 className="icon fa-home"><span className="label">Address</span></h3>
                                        Helsinki,<br />
                                        Finland
                                    </li>
                                    <li>
                                        <h3 className="icon fa-mobile"><span className="label">Phone</span></h3>
                                        ***-***-****
                                    </li>
                                    <li>
                                        <h3 className="icon fa-envelope-o"><span className="label">Email</span></h3>
                                        <a href="mailTo:tareqsolutions@gmail.com">tareqsolutions@gmail.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>

                </div>

            </Layout>
        )
    }
}

export default Testimonials